package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/1027452269/vblog/exception"
)

// API 失败 返回错误，API Exception
func Failed(c *gin.Context, err error) {
	//构造异常数据
	var resp *exception.APIException
	if e, ok := err.(*exception.APIException); ok {
		resp = e
	} else {
		resp = exception.NewAPIException(
			500, http.StatusText(http.StatusInternalServerError)).WithMessage(err.Error()).WithHttpCode(500)

	}
	// 返回异常
	c.JSON(resp.HttpCode, resp)
	// 再中断
	c.Abort()
}

// API成功,返回数据
func Success(c *gin.Context, data any) {

	c.JSON(http.StatusOK, data)

}
