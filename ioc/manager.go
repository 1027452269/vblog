package ioc

import "github.com/gin-gonic/gin"

type NameSpaceContainer struct {
	ns map[string]*Container
}

// 对象的统一管理方法
// 全局唯一的，全局变量

var nc = &NameSpaceContainer{

	ns: map[string]*Container{
		"api":        NewContainer(),
		"controller": NewContainer(),
		"config":     NewContainer(),
		"default":    NewContainer(),
	},
}

func Init() error {
	nc.Init()
	return nil
}

func Destroy() error {
	nc.Destroy()
	return nil
}

func Controller() *Container {
	return nc.ns["controller"]
}
func Api() *Container {
	return nc.ns["api"]
}

type GinApi interface {
	// 基础约束
	Object
	// 额外约束
	Registry(rr gin.IRouter)
}

func RegistryGinApi(rr gin.IRouter) {
	nc.RegistryGinApi(rr)
}

func (c *NameSpaceContainer) Init() error {
	// 遍历NameSpace
	for k := range c.ns {
		c := c.ns[k]
		// 遍历Namespace里面的对象
		for objName := range c.storage {
			obj := c.storage[objName]
			if err := obj.Init(); err != nil {
				return err
			}
		}
	}

	return nil
}

func (c *NameSpaceContainer) Destroy() error {
	// 遍历NameSpace
	for k := range c.ns {
		c := c.ns[k]
		// 遍历Namespace里面的对象
		for objName := range c.storage {
			obj := c.storage[objName]
			if err := obj.Destroy(); err != nil {
				return err
			}
		}
	}
	return nil
}

func (c *NameSpaceContainer) RegistryGinApi(rr gin.IRouter) {
	// 遍历NameSpace
	for k := range c.ns {
		c := c.ns[k]
		// 遍历Namespace里面的对象
		for objName := range c.storage {
			obj := c.storage[objName]
			if v, ok := obj.(GinApi); ok {
				v.Registry(rr)
			}
		}
	}
}
