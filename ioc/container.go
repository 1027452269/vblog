package ioc

// 设置容器

func NewContainer() *Container {
	return &Container{
		storage: map[string]Object{},
	}

}

//ioc容器具体实现
type Container struct {
	storage map[string]Object
}

func (c *Container) Registry(name string, obj Object) {
	c.storage[name] = obj
}

func (c *Container) Get(name string) any {
	return c.storage[name]
}
