package conf_test

import (
	"os"
	"testing"

	"gitlab.com/1027452269/vblog/conf"
)

func TestLoadFromFile(t *testing.T) {

	err := conf.LoadFromfile("etc/application.toml")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C())
}
func TestLoadFromEnv(t *testing.T) {
	os.Setenv("DATASOURCE_HOST", "127.0.0.2")
	err := conf.LoadFromEnv()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.C())
}
