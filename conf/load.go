package conf

import (
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

// 获取配置参数  从 文件解析 或者 环境变量处 获得

// github.com/BurntSushi/toml go里使用比较广泛的toml格式解析库
// https://github.com/BurntSushi/toml 查看该库的基本用法
// object <----> toml配置文件

func LoadFromfile(filepath string) error {
	c := DefaultConfig()
	if _, err := toml.DecodeFile(filepath, c); err != nil {
		return err
	}
	config = c
	return nil

}

//"github.com/caarlos0/env/v6" 读取环境变量

func LoadFromEnv() error {
	c := DefaultConfig()
	// env Tag
	if err := env.Parse(c); err != nil {
		return err
	}
	config = c
	// c.MySQL.Host = os.Getenv("DATASOURCE_HOST")
	return nil

}
