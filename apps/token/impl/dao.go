package impl

import (
	"context"

	"gitlab.com/1027452269/vblog/apps/token"
)

func (t *TokenServiceImpl) getToken(ctx context.Context, accessToken string) (*token.Token, error) {
	tk := token.NewToken(false)
	err := t.db.WithContext(ctx).Model(&token.Token{}).Where(" access_token = ? ", accessToken).First(tk).Error
	if err != nil {
		return nil, err
	}
	return tk, nil
}
