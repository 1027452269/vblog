package impl_test

import (
	"context"
	"testing"

	"gitlab.com/1027452269/vblog/apps/token"
	timpl "gitlab.com/1027452269/vblog/apps/token/impl"
	"gitlab.com/1027452269/vblog/apps/user"
	"gitlab.com/1027452269/vblog/apps/user/impl"
)

var (
	us  user.Service
	ts  token.Service
	ctx = context.Background()
)

func Test_IssueToken(t *testing.T) {
	tk, err := ts.IssueToken(context.Background(), token.NewIssueTokenRequest("admin", "123456"))
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestRevokeToken(t *testing.T) {

	req := token.NewRevokeTokenRequest(
		"cnset9ecrst52f5u1vp0",
		"cnset9ecrst52f5u1vpg",
	)
	tk, err := ts.RevokeToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)

}
func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("cnsmvu6crst5c7484bf0")
	tk, err := ts.ValidateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func init() {
	us = impl.NewUserServiceImpl()
	ts = timpl.NewTokenServiceImpl(us)
}
