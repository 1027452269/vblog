package impl

import (
	"gitlab.com/1027452269/vblog/apps/token"
	"gitlab.com/1027452269/vblog/apps/user"
	"gitlab.com/1027452269/vblog/conf"
	"gitlab.com/1027452269/vblog/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller().Registry(token.AppName, &TokenServiceImpl{})
}

type TokenServiceImpl struct {
	db   *gorm.DB
	user user.Service
}

func NewTokenServiceImpl(userServiceImpl user.Service) *TokenServiceImpl {
	return &TokenServiceImpl{
		db:   conf.C().DB(),
		user: userServiceImpl,
	}
}

func (token *TokenServiceImpl) Init() error {
	token.db = conf.C().DB()
	token.user = ioc.Controller().Get(user.AppName).(user.Service)
	return nil
}
func (token *TokenServiceImpl) Destroy() error {

	return nil
}
