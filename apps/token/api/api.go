package api

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/1027452269/vblog/apps/token"
	"gitlab.com/1027452269/vblog/conf"
	"gitlab.com/1027452269/vblog/ioc"
	"gitlab.com/1027452269/vblog/response"
)

func init() {
	ioc.Api().Registry(token.AppName, &TokenApiHandler{})
}

// 来实现对外提供RESTful 接口

func NewTokenApiHandler(svc token.Service) *TokenApiHandler {
	return &TokenApiHandler{
		svc: svc,
	}
}

type TokenApiHandler struct {
	svc token.Service
}

func (h *TokenApiHandler) Registry(rr gin.IRouter) {
	// 每个业务板块 都需要往Gin Engine对象注册路由
	// r := gin.Default()
	// rr := r.Group("/vblog/api/v1")

	// 模块路径
	// /vblog/api/v1/tokens

	mr := rr.Group(token.AppName)
	mr.POST("/", h.Login)
	mr.DELETE("/", h.Logout)

}

func (h *TokenApiHandler) Login(c *gin.Context) {

	//Body 必须json
	req := token.NewIssueTokenRequest("", "")
	if err := c.BindJSON(req); err != nil {
		response.Failed(c, err)
		return
	}

	// 2. 业务逻辑处理
	tk, err := h.svc.IssueToken(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	// 2.1 set cookie
	c.SetCookie(token.TOKEN_COOKIE_KEY,
		tk.AccessToken,
		tk.AccessTokenExpiredAt,
		"/",
		conf.C().Application.Domain,
		false,
		true,
	)
	// 3.返回处理的结果
	response.Success(c, tk)

}

// 退出
func (h *TokenApiHandler) Logout(c *gin.Context) {
	// 1.解析用户需求
	// token为了安全 存放在cookie获取自定义Header中
	accessToken := token.GetAccessTokenFromHttp(c.Request)
	req := token.NewRevokeTokenRequest(accessToken, c.Query("refresh_token"))
	// 2.业务逻辑处理
	_, err := h.svc.RevokeToken(c.Request.Context(), req)
	if err != nil {
		response.Failed(c, err)
		return
	}
	// 2.1 删除前端的cookie
	c.SetCookie(token.TOKEN_COOKIE_KEY, "", -1, "/", conf.C().Application.Domain, false, true)
	// 3. 返回处理的结果
	response.Success(c, "退出成功")
}

func (h *TokenApiHandler) Init() error {
	// 从Controller空间中 获取 模块的具体实现
	// 断言该实现是满足该接口的
	h.svc = ioc.Controller().Get(token.AppName).(token.Service)

	return nil
}

func (h *TokenApiHandler) Destroy() error {

	return nil
}
