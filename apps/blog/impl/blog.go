package impl

import (
	"context"

	"gitlab.com/1027452269/vblog/apps/blog"
)

// 创建博客
func (b *blogServiceImpl) CreateBlog(ctx context.Context, in *blog.CreateBlogRequest) (*blog.Blog, error) {

	return nil, nil
}

func (b *blogServiceImpl) QueryBlog(ctx context.Context, in *blog.CreateBlogRequest) (*blog.BlogSet, error) {

	return nil, nil
}

func (b *blogServiceImpl) DescribeBlog(ctx context.Context, in *blog.DescribeBlogRequest) (*blog.Blog, error) {

	return nil, nil
}
func (b *blogServiceImpl) UpdateBlog(ctx context.Context, in *blog.UpdateBlogRequest) (*blog.Blog, error) {

	return nil, nil
}

func (b *blogServiceImpl) DeleteBlog(ctx context.Context, in *blog.DeleteBlogRequest) (*blog.Blog, error) {

	return nil, nil
}

func (b *blogServiceImpl) ChangeBlogStatus(ctx context.Context, in *blog.ChangeBlogStatusRequest) (*blog.Blog, error) {

	return nil, nil
}

func (b *blogServiceImpl) AuditBlog(ctx context.Context, in *blog.AuditBlogRequest) (*blog.Blog, error) {

	return nil, nil
}
