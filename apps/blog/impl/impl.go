package impl

import (
	"gitlab.com/1027452269/vblog/apps/blog"
	"gitlab.com/1027452269/vblog/conf"
	"gitlab.com/1027452269/vblog/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller().Registry(blog.AppName, &blogServiceImpl{})
}

// blog.Service接口，是直接注册给Ioc,不需要对我暴漏
type blogServiceImpl struct {
	// 依赖了一个数据库操作的连接池对象
	db *gorm.DB
}

func (b *blogServiceImpl) Init() error {
	b.db = conf.C().DB()
	return nil
}
func (b *blogServiceImpl) Destroy() error {

	return nil
}
