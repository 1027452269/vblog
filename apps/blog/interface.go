package blog

import (
	"context"

	"github.com/go-playground/validator/v10"
	"gitlab.com/1027452269/vblog/common"
)

const (
	// 模块名称
	AppName = "blogs"
)

var (
	v = validator.New()
)

type Service interface {
	// 创建一个博客
	CreateBlog(context.Context, *CreateBlogRequest) (*Blog, error)
	// 获取一个博客列表
	QueryBlog(context.Context, *QueryBlogRequest) (*BlogSet, error)
	// 博客详情
	DescribeBlog(context.Context, *DescribeBlogRequest) (*Blog, error)
	// 更新博客内容
	UpdateBlog(context.Context, *UpdateBlogRequest) (*Blog, error)
	// 删除博客
	DeleteBlog(context.Context, *DeleteBlogRequest) (*Blog, error)
	// 文章状态修改，比如发布，草稿
	ChangeBlogStatus(context.Context, *ChangeBlogStatusRequest) (*Blog, error)
	// 文章审核 通过，未通过
	AuditBlog(context.Context, *AuditBlogRequest) (*Blog, error)
}

func NewUpdateBlogRequest(id string) *UpdateBlogRequest {

	return &UpdateBlogRequest{
		Id:                id,
		UpdateMode:        common.UPDATE_MODE_PUT,
		CreateBlogRequest: NewCreateBlogRequest(),
	}
}

type UpdateBlogRequest struct {
	//被更新的博客Id
	Id string `json:"id"`
	//更新模式
	UpdateMode common.UpdateMode `json:"update_mode"`
	// 更新的数据
	*CreateBlogRequest
}

func NewQueryBlogRequest() *QueryBlogRequest {
	return &QueryBlogRequest{
		PageSize:   20,
		PageNumber: 1,
	}
}

func NewBlogSet() *BlogSet {
	return &BlogSet{
		Items: []*Blog{},
	}
}

type BlogSet struct {
	// 总共有多少个
	Total int `json:"total"`
	// 当前查询的数据清单
	Items []*Blog `json:"items"`
}

type QueryBlogRequest struct {
	// 分页大小，一个多少个
	PageSize int
	// 当前页，查询哪一页的数据
	PageNumber int
	// 谁创建的文章
	CreateBy string
}

func (req *QueryBlogRequest) Limit() int {
	return req.PageSize
}

func (req *QueryBlogRequest) Offset() int {
	return (req.PageNumber - 1) * req.PageSize
}

func NewDescribeBlogRequest(id string) *DescribeBlogRequest {
	return &DescribeBlogRequest{
		Id: id,
	}
}

type DescribeBlogRequest struct {
	Id string
}

func NewDeleteBlogRequest(id string) *DeleteBlogRequest {
	return &DeleteBlogRequest{
		Id: id,
	}
}

type DeleteBlogRequest struct {
	Id string
}
