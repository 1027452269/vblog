package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/1027452269/vblog/apps/blog"
	"gitlab.com/1027452269/vblog/apps/user"
	"gitlab.com/1027452269/vblog/ioc"
)

func init() {
	ioc.Api().Registry(blog.AppName, &blogApiHandler{})
}

type blogApiHandler struct {
	bs blog.Service
}

func (b *blogApiHandler) Init() error {
	i := ioc.Controller().Get(blog.AppName).(blog.Service)
	b.bs = i
	return nil
}
func (b *blogApiHandler) Destroy() error {

	return nil
}
func (b *blogApiHandler) Registry(rr gin.IRouter) {

	r := rr.Group(blog.AppName)
	// 普通接口,允许访客使用，不需要权限
	r.GET("/", b.QueryBlog)
	r.GET("/:id", b.DescribeBlog)

	// 整个模块后面的请求 都需求认证
	r.Use(middleware.Auth)
	// 后面管理的接口 需要权限
	r.POST("/", b.CreateBlog)
	r.PATCH("/:id", b.PatchBlog)
	r.PUT("/:id", b.UpdateBlog)
	// 只允许管理员才能删除
	r.DELETE("/:id", middleware.Required(user.ROLE_ADMIN), b.DeleteBlog)
}
