package blog

import "github.com/infraboard/mcube/tools/pretty"

type Blog struct {
	// 用户Id
	Id int `json:"id" gorm:"column:id"`
	// 创建时间, 时间戳 10位, 秒
	CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	// 更新时间, 时间戳 10位, 秒
	UpdatedAt int64 `json:"updated_at" gorm:"column:updated_at"`
	// 用户博客参数
	*CreateBlogRequest
	// 博客状态参数
	*ChangeBlogStatusRequest
	// 博客审核状态请求参数
	*AuditBlogRequest
}

func NewAuditBlogRequest() *AuditBlogRequest {
	return &AuditBlogRequest{}
}

type AuditBlogRequest struct {
	// 审核时间
	Audit_At int64 `json:"audit_at" gorm:"column:autit_at"`
	// 是否审核通过
	IsAuditPass bool `json:"is_audit_pass" gorm:"column:is_audit_pass"`
}
type ChangeBlogStatusRequest struct {

	// 文章状态: 草稿/已发布
	Status int `json:"status" gorm:"column:status"`
	// 发布时间
	PublishedAt int64 `json:"published_at" gorm:"column:published_at"`
}

func (req *CreateBlogRequest) Validate() error {
	return v.Struct(req)
}

func NewCreateBlogRequest() *CreateBlogRequest {
	return &CreateBlogRequest{
		Tags: map[string]string{},
	}
}

type CreateBlogRequest struct {

	// 文章标题
	Title string `json:"title" gorm:"column:title" validate:"required"`
	// 作者
	Author string `json:"author" gorm:"column:author" validate:"required"`
	// 文章内容
	Content string `json:"content" gorm:"column:content" validate:"required"`
	// 文章概要信息
	Summary string `json:"summary" gorm:"column:summary"`
	// 创建人
	CreateBy string `json:"create_by" gorm:"column:create_by"`
	// 标签
	Tags map[string]string `json:"tags" gorm:"column:tags;serializer:json"`
}

func (b *Blog) TableName() string {
	return "blogs"
}

func (b *Blog) String() string {
	return pretty.ToJSON(b)
}
