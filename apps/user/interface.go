package user

import (
	"context"

	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcube/tools/pretty"
	"golang.org/x/crypto/bcrypt"
)

const (
	AppName = "users"
)

var (
	v = validator.New()
)

type Service interface {
	CreateUser(context.Context, *CreateUserRequest) (*User, error)       // 用户创建请求
	QueryUser(context.Context, *QueryUserRequest) (*UserSet, error)      // 用户查询请求
	DescriberUser(context.Context, *DescriberUserRequest) (*User, error) // 用户详情请求
}

// 用户创建请求
type CreateUserRequest struct {
	// 用户名
	Username string `json:"username" validate:"required" gorm:"column:username"`
	// 密码
	Password string `json:"password" validate:"required" gorm:"column:password"`
	// 角色
	Role Role `json:"role" gorm:"column:role"`
	// 标签
	Label map[string]string `json:"label" gorm:"column:label;serializer:json"`
}

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Role:  ROLE_VISITOR,
		Label: map[string]string{},
	}
}

// 哈希密码
func (c *CreateUserRequest) hashPassword() {
	//此处hash密码
	hp, err := bcrypt.GenerateFromPassword([]byte(c.Password), bcrypt.DefaultCost)
	if err != nil {
		return
	}
	c.Password = string(hp)
}

// 哈希密码 校验
func (c *CreateUserRequest) CheckPassword(pass string) error {

	return bcrypt.CompareHashAndPassword([]byte(c.Password), []byte(pass))

}

// 校验
func (c *CreateUserRequest) Validate() error {
	return v.Struct(c)
}
func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		PageSize:   20,
		PageNumber: 1,
	}
}

type QueryUserRequest struct {
	// 页大小
	PageSize int
	// 页码
	PageNumber int
	// 用户名
	Username string
}

func (req *QueryUserRequest) Limit() int {
	return req.PageSize
}

func (req *QueryUserRequest) Offset() int {
	return req.PageSize * (req.PageNumber - 1)
}

// 用户详情查询结构体
type DescriberUserRequest struct {
	UserId string
}

// 用户详情查询 构造函数
func NewDescriberUserRequest(uid string) *DescriberUserRequest {
	return &DescriberUserRequest{
		UserId: uid,
	}
}

type UserSet struct { // 用户集数据结构
	// 总共有多少个
	Total int64
	Items []*User
}

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}
func (u *UserSet) String() string {

	return pretty.ToJSON(u)

}
