package user

type Role int

const (
	ROLE_VISITOR Role = iota
	ROLE_ADMIN
)
