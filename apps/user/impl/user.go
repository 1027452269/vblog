package impl

import (
	"context"

	"gitlab.com/1027452269/vblog/apps/user"
)

var _ user.Service = (*UserServiceImpl)(nil)

// 用户创建
func (i *UserServiceImpl) CreateUser(ctx context.Context, in *user.CreateUserRequest) (*user.User, error) {

	// 1. 校验用户参数请求
	if err := in.Validate(); err != nil {
		return nil, err
	}

	// 2.创建用户实例对象
	u := user.NewUser(in)

	// 3.把对象持久化(放到数据库里)
	if err := i.db.WithContext(ctx).Model(&u).Create(u).Error; err != nil {
		return nil, err
	}

	// 4.返回创建好的对象
	return u, nil
}

func (i *UserServiceImpl) QueryUser(ctx context.Context, in *user.QueryUserRequest) (*user.UserSet, error) {
	// 构造一个mysql条件查询语句  select * from users where ....
	query := i.db.WithContext(ctx).Model(&user.User{})
	// 构造条件  where username = ""
	if in.Username != "" {
		query = query.Where("username = ?", in.Username)
	}
	set := user.NewUserSet()
	// 统计当前有多个
	// select COUNT(*) from users where .....
	err := query.Count(&set.Total).Error
	if err != nil {
		return nil, err
	}
	// 做真正的分页查询: sql LIMIT  offset,limit
	err = query.Limit(in.Limit()).Offset(in.Offset()).Find(&set.Items).Error
	if err != nil {
		return nil, err
	}
	return set, nil
}

func (i *UserServiceImpl) DescriberUser(ctx context.Context, in *user.DescriberUserRequest) (*user.User, error) {
	// 构造一个mysql条件查询语句  select * from users where ....
	query := i.db.WithContext(ctx).Model(&user.User{}).Where(" id = ? ", in.UserId)
	// 准备一个对象 接收数据库的返回
	u := user.NewUser(user.NewCreateUserRequest())
	if err := query.First(u).Error; err != nil {
		return nil, err
	}
	return u, nil
}
