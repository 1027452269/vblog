package impl_test

import (
	"context"
	"testing"

	"gitlab.com/1027452269/vblog/apps/user"
	"gitlab.com/1027452269/vblog/apps/user/impl"
)

var (
	i   user.Service // 声明变量
	ctx = context.Background()
)

func TestCreateUser(t *testing.T) {
	// 使用构造函数创建请求对象
	// user.CreateUserRequest{}
	req := user.NewCreateUserRequest()
	req.Username = "admin5"
	req.Password = "123456"
	req.Role = user.ROLE_ADMIN
	// 单元测试异常怎么处理
	u, err := i.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	// 自己进行期望对比，进行单元测试报错
	if u == nil {
		t.Fatal("user  not created")
	}
	// 正常打印对象
	t.Log(u)
}
func TestQueryUser(t *testing.T) {
	req := user.NewQueryUserRequest()

	u1, err := i.QueryUser(ctx, req)

	if err != nil {
		t.Fatal(err)
	}
	t.Log(u1)

}
func TestDescriberUser(t *testing.T) {
	req := user.NewDescriberUserRequest("8")

	u1, err := i.DescriberUser(ctx, req)

	if err != nil {
		t.Fatal(err)
	}
	t.Log(u1)

}
func init() {
	// 加载被测试对象, i 就是User Service接口的具体实现对象
	i = impl.NewUserServiceImpl()
}
