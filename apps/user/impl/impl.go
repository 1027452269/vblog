package impl

import (
	"gitlab.com/1027452269/vblog/apps/user"
	"gitlab.com/1027452269/vblog/conf"
	"gitlab.com/1027452269/vblog/ioc"
	"gorm.io/gorm"
)

func init() {
	ioc.Controller().Registry(user.AppName, &UserServiceImpl{})
}

type UserServiceImpl struct {
	// 依赖了一个数据库连接池对象
	db *gorm.DB
}

// 构造一个结构体
func NewUserServiceImpl() *UserServiceImpl {
	return &UserServiceImpl{
		// 获取全局的DB对象
		// 前提：配置对象准备完成
		db: conf.C().DB(),
	}
}

func (i *UserServiceImpl) Init() error {

	i.db = conf.C().DB()

	return nil
}
func (i *UserServiceImpl) Destroy() error {

	return nil

}
