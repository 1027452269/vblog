package user

import (
	"time"

	"github.com/infraboard/mcube/tools/pretty"
)

type User struct {
	Id        int   `json:"id" gorm:"column:id"`
	CreatedAt int64 `json:"created_at" gorm:"column:created_at"`
	// 创建时间，时间戳   10位  秒
	UpdatedAt int64 `json:"updated_at" gorm:"column:updated_at"`
	// 更新时间，时间戳   10位  秒

	*CreateUserRequest // 创建用户请求参数

}

func NewUser(req *CreateUserRequest) *User {
	// hash 密码
	req.hashPassword()
	return &User{
		CreatedAt:         time.Now().Unix(),
		CreateUserRequest: req,
	}

}

// 定义对象存储的表的名称
func (u *User) TableName() string {
	return "users"
}

func (u *User) String() string {
	return pretty.ToJSON(u)
}
