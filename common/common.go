package common

type UpdateMode int

const (
	// 全量更新
	UPDATE_MODE_PUT UpdateMode = iota
	// 部分更新
	UPDATE_MODE_PATCH
)
